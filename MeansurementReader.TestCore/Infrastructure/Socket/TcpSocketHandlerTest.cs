﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using MeansurementReader.InfrastructureStandard.Socket;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace MeansurementReader.TestCore.Infrastructure.Socket
{
    [TestClass]
    public class TcpSocketHandlerTest
    {
        [TestMethod]
        [Ignore]
        public async Task When_PassHostAndPortNotConnectable_Expect_ThrowsSocketException()
        {
            using (ISocketHandler socket = new TcpSocketHandler())
            {
                await Assert.ThrowsExceptionAsync<SocketException>(() => socket.ConnectAsync(IPAddress.Parse("127.0.0.1"), 5000));
            }
        }

        [TestMethod]
        public async Task When_PassHostAndPortConnectable_Expect_ConnectedPropertyValueTrue()
        {
            using (ISocketHandler socket = new TcpSocketHandler())
            {
                await socket.ConnectAsync(IPAddress.Parse("40.121.85.141"), 20013);
                Assert.IsTrue(socket.Connected);
            }
        }

        [TestMethod]
        public async Task When_PassInvalidFrameByteToServer_Expect_TimeoutException()
        {
            using (ISocketHandler socket = new TcpSocketHandler())
            {
                await socket.ConnectAsync(IPAddress.Parse("40.121.85.141"), 20004);
                Assert.IsTrue(socket.Connected);
                await Assert.ThrowsExceptionAsync<TimeoutException>(() => socket.SendAsync(Encoding.ASCII.GetBytes("test")));
            }
            await Task.Delay(60 * 1000);
        }

        [TestMethod]
        public async Task When_PassValidFrameByteToServer_Expect_NotThrowsAnyException()
        {
            using (ISocketHandler socket = new TcpSocketHandler())
            {
                await socket.ConnectAsync(IPAddress.Parse("40.121.85.141"), 20004);
                Assert.IsTrue(socket.Connected);
                await socket.SendAsync(new byte[]{ 0x7D, 0x00, 0x01, 0x01});
            }
            await Task.Delay(60 * 1000);
        }
    }
}
