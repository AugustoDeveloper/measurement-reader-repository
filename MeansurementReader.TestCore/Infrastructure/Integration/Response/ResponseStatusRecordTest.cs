﻿using MeansurementReader.InfrastructureStandard.Integration.Response;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace MeansurementReader.TestCore.Infrastructure.Integration.Response
{
    [TestClass]
    public class ResponseStatusRecordTest
    {
        [TestMethod]
        public void When_PassValidByteArray_Expect_ValidDesserializedValue()
        {
            var byteResponse = new byte[] { 0x7D, 0x04, 0x82, 0x01, 0x2C, 0x02, 0x58, 0xF1 };
            var response = new ResponseStatusRecord(byteResponse);
            response.AppendDataByte(byteResponse, true);
            Assert.IsNotNull(response);
            Assert.AreEqual(300, response.MinIndex);
            Assert.AreEqual(600, response.MaxIndex);
        }
        
    }
}
