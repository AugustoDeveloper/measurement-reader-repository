﻿using MeansurementReader.InfrastructureStandard.Integration.Response;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace MeansurementReader.TestCore.Infrastructure.Integration.Response
{
    [TestClass]
    public class ResponseEnergyValueCurrentRecordTest
    {
        [TestMethod]
        public void When_PassValidByteArray_Expect_ValidDesserializedValue()
        {
            var byteResponse = new byte[] { 0x7D, 0x04, 0x85, 0x41, 0x20, 0x00, 0x00, 0xE0 };
            var response = new ResponseEnergyValueCurrentRecord(byteResponse);
            response.AppendDataByte(byteResponse, true);
            Assert.IsNotNull(response);
            Assert.AreEqual(10.0, response.EnergyValyue);
        }
    }
}