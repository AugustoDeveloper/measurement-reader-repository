﻿using MeansurementReader.InfrastructureStandard.Integration.Response;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace MeansurementReader.TestCore.Infrastructure.Integration.Response
{
    [TestClass]
    public class ResponseDatetimeCurrentRecordTest
    {
        [TestMethod]
        public void When_PassValidByteArray_Expect_ValidDesserializedValue()
        {
            var byteResponse = new byte[] { 0x7D, 0x05, 0x84, 0x7D, 0xE1, 0xBC, 0x59, 0x2B, 0xD3 };
            var response = new ResponseDatetimeCurrentRecord(byteResponse);
            response.AppendDataByte(byteResponse, true);
            Assert.IsNotNull(response);
            Assert.AreEqual(2014, response.RecordDatetime.Year);
            Assert.AreEqual(1, response.RecordDatetime.Month);
            Assert.AreEqual(23, response.RecordDatetime.Day);
            Assert.AreEqual(17, response.RecordDatetime.Hour);
            Assert.AreEqual(25, response.RecordDatetime.Minute);
            Assert.AreEqual(10, response.RecordDatetime.Second);
        }
    }
}
