﻿using System;
using System.Net;
using System.Threading.Tasks;
using MeansurementReader.InfrastructureStandard.Socket;

namespace MeansurementReader.TestCore.Infrastructure.Integration
{
    public class MockSocketHandler : ISocketHandler
    {
        public bool Connected => throw new NotImplementedException();
        private Action<ISocketDataSource> _actionDataSource;

        public ISocketDataSource DataSource { get; set; }
        public int SendTimeoutInSeconds { get; set; }
        public int ReceiveTimeoutInSeconds { get; set; }
        public int DelayToCheckReceivedDataInMilliseconds { get; set; }

        public MockSocketHandler(Action<ISocketDataSource> actionDataSource)
        {
            _actionDataSource = actionDataSource;
        }

        public async Task<ISocketHandler> ConnectAsync(IPAddress address, int port)
        {
            return await Task.FromResult(this);
        }

        public void Dispose()
        {
            
        }

        public async Task SendAsync(byte[] buffer)
        {            
            _actionDataSource.Invoke(DataSource);
            await Task.FromResult(false);
        }
    }
}
