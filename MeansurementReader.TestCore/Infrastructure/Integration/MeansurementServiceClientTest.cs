﻿using System;
using System.Net;
using System.Threading.Tasks;
using MeansurementReader.InfrastructureStandard.Integration;
using MeansurementReader.InfrastructureStandard.Integration.Configuration;
using MeansurementReader.InfrastructureStandard.Integration.Exception;
using MeansurementReader.InfrastructureStandard.Socket;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace MeansurementReader.TestCore.Infrastructure.Integration
{
    [TestClass]
    public class MeansurementServiceClientTest
    {
        [TestMethod]
        public async Task When_PassInvalidValuesFromConfiguration_Expect_ThrowsConnectionException()
        {
            using (var service = MeansurementServiceClient
                  .CreateWithDefaultSocket(new MeansurementServiceConfiguration
                  {
                      Host = "127.0.0.1",
                      Port = 20004,
                      ReceiveTimeoutInSeconds = 10,
                      SendTimeoutInSeconds = 10
                  }))
            {
                await Assert.ThrowsExceptionAsync<ConnectionException>(() => service.GetSerialNumberAsync());
            }
        }

        [TestMethod]
        public void When_PassNullConfiguration_Expect_ThrowsConfigurationInvalidException()
        {
            Assert.ThrowsException<ConfigurationInvalidException>(() =>
            {
                using (var service = MeansurementServiceClient.CreateWithDefaultSocket(null))
                {

                }
            });
        }

        [TestMethod]
        public async Task When_SocketHandlerMockedReturnInvalidResponseCode_Expect_ThrowsInvalidResponseCodeException()
        {
            var socket = new MockSocketHandler((datasource) =>
            {
                datasource?.Receive(new byte[] { 0x7D, 0x08, 0x82, 0x41, 0x42, 0x43, 0x44, 0x45, 0x45, 0x47, 0x00, 0xC9 }, true);
            });
            using (var service = MeansurementServiceClient
                   .Create(new MeansurementServiceConfiguration
                   {
                       Host = "127.0.0.1",
                       Port = 20004,
                       ReceiveTimeoutInSeconds = 10,
                       SendTimeoutInSeconds = 10
                   }, socket))
            {
                await Assert.ThrowsExceptionAsync<InvalidResponseCodeException>(() => service.GetSerialNumberAsync());
            }
        }

        [TestMethod]
        public async Task When_SocketHandlerMockedReturnInvalidChecksumValue_Expect_ThrowsInvalidChecksumException()
        {
            var socket = new MockSocketHandler((datasource) =>
            {
                datasource?.Receive(new byte[] { 0x7D, 0x00, 0xFF, 0x1F }, true);
            });
            using (var service = MeansurementServiceClient
                   .Create(new MeansurementServiceConfiguration
                   {
                       Host = "127.0.0.1",
                       Port = 20004,
                       ReceiveTimeoutInSeconds = 10,
                       SendTimeoutInSeconds = 10
                   }, socket))
            {
                await Assert.ThrowsExceptionAsync<InvalidChecksumException>(() => service.GetSerialNumberAsync());
            }
        }

        [TestMethod]
        public async Task When_SocketHandlerMockedReturnReceivedErrorMessage_Expect_ThrowsReceivedErrorException()
        {
            var socket = new MockSocketHandler((datasource) =>
            {
                datasource?.Receive(new byte[] { 0x7D, 0x00, 0xFF, 0xFF }, true);
            });
            using (var service = MeansurementServiceClient
                   .Create(new MeansurementServiceConfiguration
                    {
                       Host = "127.0.0.1",
                       Port = 20004,
                       ReceiveTimeoutInSeconds = 10,
                       SendTimeoutInSeconds = 10
                    }, socket))
            {
                await Assert.ThrowsExceptionAsync<ReceivedErrorException>(() => service.GetSerialNumberAsync());
            }
        }

        [TestMethod]
        public async Task When_SocketHandlerMockedReturnTimeout_Expect_ThrowsTimeoutException()
        {
            var socketMock = new Mock<ISocketHandler>();
            socketMock.Setup(s => s.SendAsync(It.IsAny<byte[]>()))
                      .Throws<TimeoutException>();
            socketMock.Setup(s => s.ConnectAsync(It.IsAny<IPAddress>(), It.IsAny<int>()))
                      .Returns(Task.FromResult<ISocketHandler>(null));

            var socket = socketMock.Object;

            using (var service = MeansurementServiceClient
                   .Create(new MeansurementServiceConfiguration
                   {
                       Host = "127.0.0.1",
                       Port = 20004,
                       ReceiveTimeoutInSeconds = 10,
                       SendTimeoutInSeconds = 10
                   }, socket))
            {
                await Assert.ThrowsExceptionAsync<TimeoutException>(() => service.GetSerialNumberAsync());
            }
        }

        [TestMethod]
        public async Task When_SocketHandlerMockedReturnConnectionException_Expect_ThrowsTimeoutException()
        {
            var socketMock = new Mock<ISocketHandler>();
            socketMock.Setup(s => s.ConnectAsync(It.IsAny<IPAddress>(), It.IsAny<int>()))
                      .Throws(new ConnectionException("The mocked exception"));


            var socket = socketMock.Object;

            using (var service = MeansurementServiceClient
                   .Create(new MeansurementServiceConfiguration
                   {
                       Host = "127.0.0.1",
                       Port = 20004,
                       ReceiveTimeoutInSeconds = 10,
                       SendTimeoutInSeconds = 10
                   }, socket))
            {
                await Assert.ThrowsExceptionAsync<ConnectionException>(() => service.GetSerialNumberAsync());
            }
        }

        [TestMethod]
        public async Task When_ExecuteGetSerialNumber_Expect_NotThrowsAnyException_And_ResponseValid()
        {
            await Task.Delay(60 * 1000);
            using (var service = MeansurementServiceClient
                  .CreateWithDefaultSocket(new MeansurementServiceConfiguration
                  {
                      Host = "40.121.85.141",
                      Port = 20004,
                      ReceiveTimeoutInSeconds = 10,
                      SendTimeoutInSeconds = 10
                  }))
            {
                var response = await service.GetSerialNumberAsync();
                Assert.IsNotNull(response);
                Assert.IsNotNull(response.Data);
                Assert.IsFalse(string.IsNullOrEmpty(response.Data));
            }
        }

        [TestMethod]
        public async Task When_ExecuteGetStatusRecord_Expect_NotThrowsAnyException_And_ResponseValid()
        {
            await Task.Delay(60 * 1000);
            using (var service = MeansurementServiceClient
                  .CreateWithDefaultSocket(new MeansurementServiceConfiguration
                  {
                      Host = "40.121.85.141",
                      Port = 20004,
                      ReceiveTimeoutInSeconds = 10,
                      SendTimeoutInSeconds = 10
                  }))
            {
                var response = await service.GetStatusRecordAsync();
                Assert.IsNotNull(response);
                Assert.AreNotEqual(response.MinIndex, 0);
                Assert.AreNotEqual(response.MaxIndex, 0);
            }
        }

        [TestMethod]
        public async Task When_ExecuteMarkIndexToRead_Expect_NotThrowsAnyException_And_ResponseValid()
        {
            using (var service = MeansurementServiceClient
                  .CreateWithDefaultSocket(new MeansurementServiceConfiguration
                  {
                      Host = "40.121.85.141",
                      Port = 20005,
                      ReceiveTimeoutInSeconds = 10,
                      SendTimeoutInSeconds = 10
                  }))
            {
                var responseSerialNumber = await service.GetSerialNumberAsync();
                Assert.IsNotNull(responseSerialNumber);
                Assert.IsNotNull(responseSerialNumber.Data);
                Assert.IsFalse(string.IsNullOrEmpty(responseSerialNumber.Data));
                var responseStatusRecord = await service.GetStatusRecordAsync();
                Assert.AreNotEqual(responseStatusRecord.MinIndex, 0);
                Assert.AreNotEqual(responseStatusRecord.MaxIndex, 0);

                var response = await service.MarkIndexToRead(responseStatusRecord.MaxIndex);
                Assert.IsNotNull(response);
                Assert.IsTrue(response.IsSuccess);
            }
        }

        [TestMethod]
        public async Task When_ExecuteRequestDatetimeCurrentRecord_Expect_NotThrowsAnyException_And_ResponseValid()
        {
            using (var service = MeansurementServiceClient
                  .CreateWithDefaultSocket(new MeansurementServiceConfiguration
                  {
                      Host = "40.121.85.141",
                      Port = 20005,
                      ReceiveTimeoutInSeconds = 10,
                      SendTimeoutInSeconds = 10
                  }))
            {
                var responseSerialNumber = await service.GetSerialNumberAsync();
                Assert.IsNotNull(responseSerialNumber);
                Assert.IsNotNull(responseSerialNumber.Data);
                Assert.IsFalse(string.IsNullOrEmpty(responseSerialNumber.Data));
                var responseStatusRecord = await service.GetStatusRecordAsync();
                Assert.AreNotEqual(responseStatusRecord.MinIndex, 0);
                Assert.AreNotEqual(responseStatusRecord.MaxIndex, 0);

                var responseRecordReaded = await service.MarkIndexToRead(responseStatusRecord.MaxIndex);
                Assert.IsNotNull(responseRecordReaded);
                Assert.IsTrue(responseRecordReaded.IsSuccess);

                var response = await service.GetDatetimeFromCurrentRecord();
                Assert.IsNotNull(response);
            }
        }

        [TestMethod]
        public async Task When_ExecuteRequestEnergyValueCurrentRecord_Expect_NotThrowsAnyException_And_ResponseValid()
        {
            using (var service = MeansurementServiceClient
                  .CreateWithDefaultSocket(new MeansurementServiceConfiguration
                  {
                      Host = "40.121.85.141",
                      Port = 20005,
                      ReceiveTimeoutInSeconds = 10,
                      SendTimeoutInSeconds = 10
                  }))
            {
                var responseSerialNumber = await service.GetSerialNumberAsync();
                Assert.IsNotNull(responseSerialNumber);
                Assert.IsNotNull(responseSerialNumber.Data);
                Assert.IsFalse(string.IsNullOrEmpty(responseSerialNumber.Data));
                var responseStatusRecord = await service.GetStatusRecordAsync();
                Assert.AreNotEqual(responseStatusRecord.MinIndex, 0);
                Assert.AreNotEqual(responseStatusRecord.MaxIndex, 0);

                var responseRecordReaded = await service.MarkIndexToRead(responseStatusRecord.MaxIndex);
                Assert.IsNotNull(responseRecordReaded);
                Assert.IsTrue(responseRecordReaded.IsSuccess);

                var responseDatetimeFromRecord = await service.GetDatetimeFromCurrentRecord();
                Assert.IsNotNull(responseDatetimeFromRecord);

                var response = await service.GetEnergyValueFromCurrentRecord();
                Assert.IsNotNull(response);
                Assert.IsTrue(response.EnergyValyue > 0);
            }
        }
    }
}
