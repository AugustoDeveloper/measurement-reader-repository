﻿using System;
using System.IO;
using System.Threading.Tasks;
using MeansurementReader.Infrastructure.Integration;
using MeansurementReader.Infrastructure.Integration.Configuration;
using MeansurementReader.Infrastructure.Integration.Exception;
using MeansurementReader.Infrastructure.IO;
using MeansurementReader.UI.IO;
using MeansurementReader.UI.Model;

namespace MeansurementReader.UI
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            MainAsync(args).Wait();
        }

        public static async Task MainAsync(string[] args)
        {
            if (args.Length < 1)
            {
                Console.Error.WriteLine($"the argument file not passed");
                Console.ReadKey();
                return;
            }

            var filename = args[0];
            if (string.IsNullOrEmpty(filename) || File.Exists(filename) == false)
            {
                Console.Error.WriteLine($"the \"{filename}\" is incorrect or not exists ");
                Console.ReadKey();
                return;
            }

            var baseDir = Path.GetDirectoryName(Path.GetFullPath(filename));

            var input = new CsvSolicitationList<Solicitation>();
            var solicitations = await input.Load(filename, true);

            foreach (var solicitation in solicitations.Solicitations)
            {
                using (var service = MeansurementServiceClient.CreateWithDefaultSocket(new MeansurementServiceConfiguration { Host = solicitation.Ip, Port = solicitation.Port, ReceiveTimeoutInSeconds = 10, SendTimeoutInSeconds = 10 }))
                {
                    try
                    {
                        var serialNumber = await service.GetSerialNumberAsync();
                        var output = new CsvOutputMeansurement(serialNumber.SerialNumber);

                        var statusRecord = await service.GetStatusRecordAsync();

                        for (int i = solicitation.StartIndex; i <= solicitation.EndIndex; i++)
                        {
                            var statusIndex = await service.MarkIndexToRead((ushort)i);
                            if (statusIndex.IsSuccess)
                            {
                                var currentRecordDatetime = await service.GetDatetimeFromCurrentRecord();
                                var currentRecordEnergyValue = await service.GetEnergyValueFromCurrentRecord();

                                output.AppendLine(new MeansurementModel
                                {
                                    Index = i,
                                    EnergyValue = currentRecordEnergyValue.EnergyValyue,
                                    RecordDatetime = currentRecordDatetime.RecordDatetime
                                });
                            }
                        }


                        output.Save($"{baseDir}/{Guid.NewGuid().ToString()}.csv");
                    }
                    catch(BaseServiceException)
                    {
                        
                    }
                }
            }
        }
    }
}
