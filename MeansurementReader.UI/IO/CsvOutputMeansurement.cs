﻿using System.IO;
using System.Text;
using MeansurementReader.Infrastructure.IO;

namespace MeansurementReader.UI.IO
{
    public class CsvOutputMeansurement : IOutputMeansurement
    {
        private StringBuilder _buffer = new StringBuilder();

        public CsvOutputMeansurement(string header)
        {
            _buffer.AppendLine(header);
        }

        public void AppendLine(IMeansurementModel meansurement)
        {
            _buffer.AppendLine($"{meansurement.Index};{meansurement.RecordDatetimeFormated};{meansurement.EnergyValueFormated}");
        }

        public void Save(string filename)
        {
            File.AppendAllText(filename, _buffer.ToString());
        }
    }
}