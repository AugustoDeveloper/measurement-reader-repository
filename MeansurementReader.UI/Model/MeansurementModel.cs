﻿using System;
using System.Globalization;
using MeansurementReader.Infrastructure.IO;

namespace MeansurementReader.UI.Model
{
    public class MeansurementModel : IMeansurementModel
    {
        public int Index { get; set; }

        public DateTime RecordDatetime { get; set; }

        public string EnergyValueFormated => Math.Round(EnergyValue, mode: MidpointRounding.ToEven).ToString("F", new NumberFormatInfo { NumberDecimalSeparator = "," });

        public float EnergyValue { get; set; }

        public string RecordDatetimeFormated => RecordDatetime.ToString("yyyy-MM-dd HH:mm:ss");
    }
}
