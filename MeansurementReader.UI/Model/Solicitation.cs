﻿using MeansurementReader.Infrastructure.IO;

namespace MeansurementReader.UI.Model
{
    public class Solicitation : ISolicitationModel
    {
        public string Ip { get; set; }
        public int Port  { get; set; }
        public int StartIndex { get; set; }
        public int EndIndex { get; set; }
    }
}
