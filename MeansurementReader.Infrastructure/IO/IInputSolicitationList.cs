﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MeansurementReader.Infrastructure.IO
{
    public interface IInputSolicitationList<TSolicitationModel>
        where TSolicitationModel : class, ISolicitationModel, new()
    {
        List<TSolicitationModel> Solicitations { get; }

        Task<IInputSolicitationList<TSolicitationModel>> Load(string filename, bool ignoreLineErrors);
    }
}
