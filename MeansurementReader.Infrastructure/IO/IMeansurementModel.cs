﻿using System;
namespace MeansurementReader.Infrastructure.IO
{
    public interface IMeansurementModel
    {
        int Index { get; }
        DateTime RecordDatetime { get; }
        string EnergyValueFormated { get; }
        float EnergyValue { get; }
        string RecordDatetimeFormated { get; }
    }
}
