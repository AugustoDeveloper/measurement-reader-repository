﻿using System;
namespace MeansurementReader.Infrastructure.IO
{
    public interface IOutputMeansurement
    {
        void AppendLine(IMeansurementModel meansurement);
        void Save(string filename);
    }
}
