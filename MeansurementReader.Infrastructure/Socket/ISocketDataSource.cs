﻿using System;
namespace MeansurementReader.Infrastructure.Socket
{
    public interface ISocketDataSource
    {
        void Receive(byte[] data, bool isCompleteData);
    }
}
