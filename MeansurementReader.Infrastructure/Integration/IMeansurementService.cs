﻿using System;
using System.Threading.Tasks;
using MeansurementReader.Infrastructure.Integration.Response;

namespace MeansurementReader.Infrastructure.Integration
{
    public interface IMeansurementService : IDisposable
    {
        Task<ResponseSerialNumber> GetSerialNumberAsync();
        Task<ResponseStatusRecord> GetStatusRecordAsync();
        Task<ResponseIndexReaded> MarkIndexToRead(ushort index);
        Task<ResponseDatetimeCurrentRecord> GetDatetimeFromCurrentRecord();
        Task<ResponseEnergyValueCurrentRecord> GetEnergyValueFromCurrentRecord();
    }
}