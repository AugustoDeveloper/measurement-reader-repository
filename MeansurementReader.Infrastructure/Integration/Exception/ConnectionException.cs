﻿using System;
namespace MeansurementReader.Infrastructure.Integration.Exception
{
    public class ConnectionException: System.Exception
    {
        public ConnectionException(string message) : base(message) { }
    }
}
