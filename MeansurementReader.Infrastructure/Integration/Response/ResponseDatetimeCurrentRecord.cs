﻿using System;
using System.Text;

namespace MeansurementReader.Infrastructure.Integration.Response
{
    public class ResponseDatetimeCurrentRecord : BaseResponseMessage
    {
        public override byte ExpectedCode => 0x84;
        public DateTime RecordDatetime { get; private set; }

        public ResponseDatetimeCurrentRecord(byte[] response) : base(response){ }

        protected override void LoadData()
        {
            var stringBuffer = new StringBuilder();
            foreach (var item in DataBytes)
            {
                string binary = Convert.ToString(item, 2).PadLeft(8, '0');
                stringBuffer.Append(binary);
            }

            if (stringBuffer.Length > 0)
            {
                string completeBinary = stringBuffer.ToString();
                string yearBinary = completeBinary.Substring(0, 12);
                string monthBinary = completeBinary.Substring(12, 4);
                string dayBinary = completeBinary.Substring(16, 5);
                string hourBinary = completeBinary.Substring(21, 5);
                string minuteBinary = completeBinary.Substring(26, 6);
                string secondBinary = completeBinary.Substring(32, 6);

                int year = Convert.ToInt32(yearBinary, 2);
                int month = Convert.ToInt32(monthBinary, 2);
                int day = Convert.ToInt32(dayBinary, 2);
                int hour  = Convert.ToInt32(hourBinary, 2);
                int minute = Convert.ToInt32(minuteBinary, 2);
                int second = Convert.ToInt32(secondBinary, 2);

                RecordDatetime = new DateTime(year, month, day, hour, minute, second);
            }
        }
    }
}
