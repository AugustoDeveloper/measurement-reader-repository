﻿using System.Collections.Generic;
using MeansurementReader.Infrastructure.Integration.Exception;

namespace MeansurementReader.Infrastructure.Integration.Response
{

    public abstract class BaseResponseMessage
    {
        #region Constants

        public const byte Header = 0x7D;
        public const byte ErrorCode = 0xFF;

        #endregion

        #region Fields

        private List<byte> _dataBytesList;

        #endregion

        #region Properties

        public abstract byte ExpectedCode { get; }
        public byte Code { get; private set; }
        public int Length { get; }
        protected byte[] DataBytes { get; private set; }
        public int Checksum { get; private set; }
        public bool IsValidChecksum { get; private set; }
        public virtual int? ExpectedLength { get; }

        #endregion

        #region Constructors

        protected BaseResponseMessage(byte[] response)
        {
            byte responseHeader = response[0];
            Length = response[1];
            Code = response[2];
            _dataBytesList = new List<byte>();
        }

        #endregion

        #region Methods

        public void AppendDataByte(byte[] response, bool isLastByteArray)
        {
            if (isLastByteArray == false)
            {
                _dataBytesList.AddRange(response);
            }
            else
            {
                var buffer = _dataBytesList.Count > 0 ? _dataBytesList.ToArray() : response;
                var lastPositionDataByte = Length + 2;
                Checksum = response[lastPositionDataByte + 1];

                int validateChecksum = Length ^ Code;
                DataBytes = new byte[Length];
                for (int i = 3; i <= lastPositionDataByte; i++)
                {
                    DataBytes[i - 3] = buffer[i];
                    validateChecksum ^= buffer[i];
                }

                IsValidChecksum = validateChecksum == Checksum;
                Validate();
                LoadData();
            }
        }

        protected abstract void LoadData();

        protected virtual void Validate()
        {
            if (IsValidChecksum == false)
            {
                throw new InvalidChecksumException();
            }

            if (ExpectedCode != Code)
            {
                if (Code == ErrorCode)
                {
                    throw new ReceivedErrorException();
                }

                throw new InvalidResponseCodeException();
            }

            if (ExpectedLength.HasValue)
            {
                if (ExpectedLength != Length)
                {
                    throw new InvalidLengthException();
                }

                if (Length != DataBytes.Length)
                {
                    throw new InvalidDataLengthException();
                }
            }
        }

        public override string ToString()
        {
            return $"Length: {Length}, Data: {DataBytes}, Checksum: {Checksum}, IsValidChecksum: {IsValidChecksum}";
        }

        #endregion

    }
}
