﻿using System;
using System.Text;

namespace MeansurementReader.Infrastructure.Integration.Response
{
    public class ResponseSerialNumber : BaseResponseMessage
    {
        public string SerialNumber { get; private set; }
        public ResponseSerialNumber(byte[] response) : base(response) {}

        public override byte ExpectedCode => 0x81;

        protected override void LoadData()
        {            
            SerialNumber = Encoding.ASCII.GetString(DataBytes);
            SerialNumber = SerialNumber.Replace("\0", string.Empty);
        }
    }
}
