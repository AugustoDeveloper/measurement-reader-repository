﻿using System;
namespace MeansurementReader.Infrastructure.Integration.Response
{
    public class ResponseIndexReaded : BaseResponseMessage
    {
        public override byte ExpectedCode => 0x83;
        public bool IsSuccess { get; private set; }

        public ResponseIndexReaded(byte[] response) : base(response) { }

        protected override void LoadData()
        {
            IsSuccess = DataBytes[0] == 0;
        }
    }
}
