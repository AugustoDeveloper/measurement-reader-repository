﻿using System;
namespace MeansurementReader.Infrastructure.Integration.Configuration
{
    public class MeansurementServiceConfiguration: IMeansurementServiceConfiguration
    {
        public int Port { get; set; }

        public string Host { get; set; }

        public int SendTimeoutInSeconds { get; set; }

        public int ReceiveTimeoutInSeconds { get; set; }
    }
}
