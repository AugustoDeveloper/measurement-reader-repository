﻿using System;
namespace MeansurementReader.Infrastructure.Integration.Configuration
{
    public interface IMeansurementServiceConfiguration
    {
        int Port { get; }
        string Host { get; }
        int SendTimeoutInSeconds { get; }
        int ReceiveTimeoutInSeconds { get; }
    }
}
