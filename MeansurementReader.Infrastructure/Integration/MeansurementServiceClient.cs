﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Threading.Tasks;
using MeansurementReader.Infrastructure.Socket;
using MeansurementReader.Infrastructure.Integration.Configuration;
using MeansurementReader.Infrastructure.Integration.Exception;
using MeansurementReader.Infrastructure.Integration.Request;
using MeansurementReader.Infrastructure.Integration.Response;

namespace MeansurementReader.Infrastructure.Integration
{
    public sealed class MeansurementServiceClient : IMeansurementService, ISocketDataSource
    {
        #region Fields

        private ISocketHandler _socket;
        private readonly IMeansurementServiceConfiguration _configuration;
        private bool _disposed;
        private BaseResponseMessage _currentResponse;
        private Type _currentTypeResponseMessage;

        #endregion

        #region Constructors

        private MeansurementServiceClient(IMeansurementServiceConfiguration configuration, ISocketHandler socket)
        {
            _socket = socket;
            _configuration = configuration ?? throw new ConfigurationInvalidException();
            InitializeSocket();
        }

        public static IMeansurementService CreateWithDefaultSocket(IMeansurementServiceConfiguration configuration)
        {
            return new MeansurementServiceClient(configuration, new TcpSocketHandler());
        }

        public static IMeansurementService Create(IMeansurementServiceConfiguration configuration, ISocketHandler socket)
        {
            return new MeansurementServiceClient(configuration, socket);
        }

        #endregion

        #region Methods

        private void InitializeSocket()
        {
            _socket.ReceiveTimeoutInSeconds = _configuration.ReceiveTimeoutInSeconds;
            _socket.SendTimeoutInSeconds = _configuration.SendTimeoutInSeconds;
            _socket.DataSource = this;
        }

        public async Task<ResponseStatusRecord> GetStatusRecordAsync()
        {
            var request = new RequestStatusRecord();
            return await RequestToServerAsync<RequestStatusRecord, ResponseStatusRecord>(request);
        }

        private async Task<TResponse> RequestToServerAsync<TRequest, TResponse>(TRequest request)
            where TRequest : struct, IRequestMessage
            where TResponse : BaseResponseMessage
        {
            _currentResponse = null;
            _currentTypeResponseMessage = typeof(TResponse);

            try
            {
                await _socket.ConnectAsync(IPAddress.Parse(_configuration.Host), _configuration.Port);
            }
            catch (System.Exception ex)
            {
                throw new ConnectionException(ex.Message);
            }

            await _socket.SendAsync(request.GetBytes());
            return (TResponse)_currentResponse;
        }

        public async Task<ResponseSerialNumber> GetSerialNumberAsync()
        {
            var request = new RequestSerialNumber();
            return await RequestToServerAsync<RequestSerialNumber, ResponseSerialNumber>(request);
        }


        public async Task<ResponseIndexReaded> MarkIndexToRead(ushort index)
        {
            var request = new RequestMarkIndexToRead(index);
            return await RequestToServerAsync<RequestMarkIndexToRead, ResponseIndexReaded>(request);
        }

        public async Task<ResponseDatetimeCurrentRecord> GetDatetimeFromCurrentRecord()
        {
            var request = new RequestDatetimeCurrentRecord();
            return await RequestToServerAsync<RequestDatetimeCurrentRecord, ResponseDatetimeCurrentRecord>(request);
        }

        public async Task<ResponseEnergyValueCurrentRecord> GetEnergyValueFromCurrentRecord()
        {
            var request = new RequestEnergyValueCurrentValue();
            return await RequestToServerAsync<RequestEnergyValueCurrentValue, ResponseEnergyValueCurrentRecord>(request);
        }

        public void Receive(byte[] data, bool isCompleteData)
        {
            _currentResponse = _currentResponse ?? (BaseResponseMessage)Activator.CreateInstance(_currentTypeResponseMessage, data);
            _currentResponse.AppendDataByte(data, isCompleteData);
        }

        private void Dispose(bool disposing)
        {
            if (disposing && _disposed == false)
            {
                _disposed = true;
                _socket.Dispose();
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        #endregion
    }
}
