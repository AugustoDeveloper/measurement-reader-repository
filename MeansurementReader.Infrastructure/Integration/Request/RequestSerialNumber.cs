﻿using System;
using System.Runtime.InteropServices;

namespace MeansurementReader.Infrastructure.Integration.Request
{
    [StructLayout(LayoutKind.Sequential)]
    public struct RequestSerialNumber : IRequestMessage
    {
        public const byte Header = 0x7D;
        public const byte Length = 0x00;
        public const byte Code = 0x01;
        public const int Checksum = Length ^ Code;


        public byte[] GetBytes()
        {
            return new byte[] { Header, Length, Code, Convert.ToByte(Checksum) };
        }
    }
}
