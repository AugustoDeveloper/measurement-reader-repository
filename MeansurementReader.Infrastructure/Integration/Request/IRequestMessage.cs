﻿namespace MeansurementReader.Infrastructure.Integration.Request
{
    public interface IRequestMessage
    {
        byte[] GetBytes();
    }
}