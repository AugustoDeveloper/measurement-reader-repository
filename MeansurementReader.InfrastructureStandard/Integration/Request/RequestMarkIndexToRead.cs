﻿using System;
using System.Runtime.InteropServices;

namespace MeansurementReader.InfrastructureStandard.Integration.Request
{
    [StructLayout(LayoutKind.Sequential)]
    public struct RequestMarkIndexToRead : IRequestMessage
    {
        public const byte Header = 0x7D;
        public const byte Code = 0x03;
        public const byte Length = 0x02;
        public int Checksum;
        public ushort Index;
        private byte[] _byteArrayIndex;

        public RequestMarkIndexToRead(ushort index)
        {
            Index = index;
            _byteArrayIndex = BitConverter.GetBytes(index);
            Checksum = Code ^ Length ^ _byteArrayIndex[1] ^ _byteArrayIndex[0];
        }

        public byte[] GetBytes()
        {
            return new byte[] { Header, Length, Code, _byteArrayIndex[1], _byteArrayIndex[0], Convert.ToByte(Checksum) };   
        }
    }
}
