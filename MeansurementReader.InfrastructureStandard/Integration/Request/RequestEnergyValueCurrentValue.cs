﻿using System;
using System.Runtime.InteropServices;

namespace MeansurementReader.InfrastructureStandard.Integration.Request
{
    [StructLayout(LayoutKind.Sequential)]
    public struct RequestEnergyValueCurrentValue : IRequestMessage
    {
        public const byte Header = 0x7D;
        public const byte Length = 0x00;
        public const byte Code = 0x05;
        public const int Checksum = Length ^ Code;

        public byte[] GetBytes()
        {
            return new byte[] { Header, Length, Code, Convert.ToByte(Checksum) };
        }
    }
}
