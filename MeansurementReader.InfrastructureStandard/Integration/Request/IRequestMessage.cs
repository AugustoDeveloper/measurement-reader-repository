﻿namespace MeansurementReader.InfrastructureStandard.Integration.Request
{
    public interface IRequestMessage
    {
        byte[] GetBytes();
    }
}