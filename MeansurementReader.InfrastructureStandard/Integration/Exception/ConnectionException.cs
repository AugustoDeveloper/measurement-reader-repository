﻿using System;
namespace MeansurementReader.InfrastructureStandard.Integration.Exception
{
    public class ConnectionException : System.Exception
    {
        public ConnectionException(string message) : base(message) { }
    }
}
