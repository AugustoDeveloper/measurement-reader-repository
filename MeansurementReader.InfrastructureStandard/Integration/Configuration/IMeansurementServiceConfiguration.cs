﻿using System;
namespace MeansurementReader.InfrastructureStandard.Integration.Configuration
{
    public interface IMeansurementServiceConfiguration
    {
        int Port { get; }
        string Host { get; }
        int SendTimeoutInSeconds { get; }
        int ReceiveTimeoutInSeconds { get; }
    }
}
