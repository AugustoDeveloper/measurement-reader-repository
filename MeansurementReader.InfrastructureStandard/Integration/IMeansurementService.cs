﻿using System;
using System.Threading.Tasks;
using MeansurementReader.InfrastructureStandard.Integration.Response;

namespace MeansurementReader.InfrastructureStandard.Integration
{
    public interface IMeansurementService : IDisposable
    {
        Task<ResponseSerialNumber> GetSerialNumberAsync();
        Task<ResponseStatusRecord> GetStatusRecordAsync();
        Task<ResponseIndexReaded> MarkIndexToRead(ushort index);
        Task<ResponseDatetimeCurrentRecord> GetDatetimeFromCurrentRecord();
        Task<ResponseEnergyValueCurrentRecord> GetEnergyValueFromCurrentRecord();
    }
}