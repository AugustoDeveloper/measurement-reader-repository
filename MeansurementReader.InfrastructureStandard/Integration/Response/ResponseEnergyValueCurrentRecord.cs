﻿using System;
using System.Text;

namespace MeansurementReader.InfrastructureStandard.Integration.Response
{
    public class ResponseEnergyValueCurrentRecord : BaseResponseMessage
    {
        public override byte ExpectedCode => 0x85;
        public float EnergyValyue { get; private set; }

        public ResponseEnergyValueCurrentRecord(byte[] response) : base(response)
        {
        }

        protected override void LoadData()
        {
            EnergyValyue = BitConverter.ToSingle(new byte[]{ DataBytes[3], DataBytes[2], DataBytes[1], DataBytes[0] }, 0);
        }
    }
}
