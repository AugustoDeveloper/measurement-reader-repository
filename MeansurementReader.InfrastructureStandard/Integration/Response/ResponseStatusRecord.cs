﻿using System;

namespace MeansurementReader.InfrastructureStandard.Integration.Response
{
    public class ResponseStatusRecord : BaseResponseMessage
    {
        public ushort MinIndex { get; private set; }
        public ushort MaxIndex { get; private set; }
        public override byte ExpectedCode => 0x82;

        public ResponseStatusRecord(byte[] response) : base(response)
        {
        }

        protected override void Validate()
        {
            base.Validate();
            if (Length != DataBytes.Length)
            {
                
            }

            if (DataBytes.Length != 4)
            {
                
            }
        }

        protected override void LoadData()
        {
            MinIndex = BitConverter.ToUInt16(new byte[] { DataBytes[1], DataBytes[0] }, 0);
            MaxIndex = BitConverter.ToUInt16(new byte[] { DataBytes[3], DataBytes[2] }, 0);
        }
    }
}
