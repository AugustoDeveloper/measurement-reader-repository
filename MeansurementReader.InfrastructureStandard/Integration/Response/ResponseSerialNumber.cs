﻿using System;
using System.Text;

namespace MeansurementReader.InfrastructureStandard.Integration.Response
{
    public class ResponseSerialNumber : BaseResponseMessage
    {
        public string Data { get; private set; }
        public ResponseSerialNumber(byte[] response) : base(response) {}

        public override byte ExpectedCode => 0x81;

        protected override void LoadData()
        {
            Data = Encoding.ASCII.GetString(DataBytes);
        }
    }
}
