﻿using System;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

namespace MeansurementReader.InfrastructureStandard.Socket
{
    public interface ISocketHandler : IDisposable
    {
        #region Properties

        bool Connected { get; }
        ISocketDataSource DataSource { get; set; }
        int SendTimeoutInSeconds { get; set; }
        int ReceiveTimeoutInSeconds { get; set; }
        int DelayToCheckReceivedDataInMilliseconds { get; set; }

        #endregion

        #region Methods

        Task<ISocketHandler> ConnectAsync(IPAddress address, int port);
        Task SendAsync(byte[] buffer);

        #endregion
    }
}
