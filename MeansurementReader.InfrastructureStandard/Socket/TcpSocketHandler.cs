﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;

namespace MeansurementReader.InfrastructureStandard.Socket
{
    public sealed class TcpSocketHandler : ISocketHandler
    {
        #region Fields

        private bool _disposed;
        private readonly TcpClient _tcpClientSocket;
        private NetworkStream _socketStream;

        #endregion

        #region Properties

        public ISocketDataSource DataSource { get; set; }
        public bool Connected => _tcpClientSocket != null && _tcpClientSocket.Connected;

        public int SendTimeoutInSeconds { get; set; } = 10;
        public int ReceiveTimeoutInSeconds { get; set; } = 10;
        public int DelayToCheckReceivedDataInMilliseconds { get; set; } = 200;

        #endregion

        #region Constructors

        public TcpSocketHandler()
        {
            _tcpClientSocket = new TcpClient();
        }

        #endregion

        #region Methods

        /// <summary>
        /// Connect to server
        /// </summary>
        /// <returns>The async.</returns>
        /// <param name="address">Address.</param>
        /// <param name="port">Port.</param>
        public async Task<ISocketHandler> ConnectAsync(IPAddress address, int port)
        {
            if (_tcpClientSocket.Connected)
            {
                return this;
            }

            await _tcpClientSocket.ConnectAsync(address, port);
            _socketStream = _tcpClientSocket.GetStream();
            return this;
        }

        /// <summary>
        /// Sends the byte to server.
        /// </summary>
        /// <returns>The async.</returns>
        /// <param name="buffer">Buffer.</param>
        public async Task SendAsync(byte[] buffer)
        {
            _tcpClientSocket.SendTimeout = SendTimeoutInSeconds * 1000;
            await _socketStream.WriteAsync(buffer, 0, buffer.Length);
            await CheckIfHasDataAvailable();
        }

        /// <summary>
        /// Checks if has data available and wait data is available. 
        /// </summary>
        /// <returns>the async task.</returns>
        private async Task CheckIfHasDataAvailable()
        {
            _tcpClientSocket.ReceiveTimeout = ReceiveTimeoutInSeconds * 1000;
            var timeout = DateTime.Now.AddSeconds(ReceiveTimeoutInSeconds);

            while (_disposed == false)
            {
                if (timeout < DateTime.Now)
                {
                    throw new TimeoutException();
                }

                while (_socketStream.DataAvailable)
                {
                    byte[] receivedByte = new byte[256];
                    await _socketStream.ReadAsync(receivedByte, 0, receivedByte.Length);

                    DataSource?.Receive(receivedByte, !_socketStream.DataAvailable);
                    if (_socketStream.DataAvailable == false)
                    {
                        return;
                    }
                }

                await Task.Delay(DelayToCheckReceivedDataInMilliseconds);
            }
        }

        private void Dispose(bool disposing)
        {
            if (disposing && _disposed == false)
            {
                _disposed = true;
                _tcpClientSocket.Close();
                _tcpClientSocket.Dispose();
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        #endregion
    }
}