﻿using System;
namespace MeansurementReader.InfrastructureStandard.Socket
{
    public interface ISocketDataSource
    {
        void Receive(byte[] data, bool isCompleteData);
    }
}
