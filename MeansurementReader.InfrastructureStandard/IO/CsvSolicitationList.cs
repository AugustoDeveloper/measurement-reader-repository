﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using MeansurementReader.InfrastructureStandard.IO.Exception;

namespace MeansurementReader.InfrastructureStandard.IO
{
    public class CsvSolicitationList<TSolicitationModel> : IInputSolicitationList<TSolicitationModel>
        where TSolicitationModel : class, ISolicitationModel, new()
    {
        public List<TSolicitationModel> Solicitations { get; }

        public CsvSolicitationList()
        {
            Solicitations = new List<TSolicitationModel>();
        }

        public async Task<IInputSolicitationList<TSolicitationModel>> Load(string filename, bool ignoreLineErrors = false)
        {
            using(var file = new StreamReader(filename))
            {
                while(file.EndOfStream == false)
                {
                    try
                    {
                        var commaText = await file.ReadLineAsync();
                        var modelTextArray = commaText.Split(';');

                        var tupleSolictionation = Validate(modelTextArray);
                        Solicitations.Add(new TSolicitationModel
                        {
                            Ip = tupleSolictionation.ipAddress,
                            Port = tupleSolictionation.port,
                            StartIndex = tupleSolictionation.startIndex,
                            EndIndex = tupleSolictionation.endIndex
                        });
                    }
                    catch
                    {
                        if (ignoreLineErrors)
                        {
                            continue;
                        }
                        
                        throw;
                    }
                }
            }
            return this;
        }

        private (string ipAddress, int port, int startIndex, int endIndex) Validate(string[] modelTextArray)
        {
            if (modelTextArray.Length != 4)
            {
                throw new InvalidFormatSolicitationsException();
            }

            bool isValidated = string.IsNullOrEmpty(modelTextArray[0]);
            isValidated &= string.IsNullOrEmpty(modelTextArray[1]);
            isValidated &= string.IsNullOrEmpty(modelTextArray[2]);
            isValidated &= string.IsNullOrEmpty(modelTextArray[3]);
            if (isValidated == false)
            {
                throw new InvalidFormatSolicitationsException();
            }

            return (modelTextArray[0], Convert.ToInt32(modelTextArray[1]), Convert.ToInt32(modelTextArray[2]), Convert.ToInt32(modelTextArray[3]));
        }
    }
}
