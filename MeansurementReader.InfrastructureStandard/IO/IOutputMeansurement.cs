﻿using System;
namespace MeansurementReader.InfrastructureStandard.IO
{
    public interface IOutputMeansurement
    {
        void WriteLine(IMeansurementModel meansurement);
    }
}
