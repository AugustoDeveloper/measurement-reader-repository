﻿namespace MeansurementReader.InfrastructureStandard.IO
{
    public interface ISolicitationModel
    {
        string Ip { get; set; }
        int Port { get; set; }
        int StartIndex { get; set; }
        int EndIndex { get; set; }
    }
}
