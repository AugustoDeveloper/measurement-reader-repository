﻿using System;
namespace MeansurementReader.InfrastructureStandard.IO
{
    public interface IMeansurementModel
    {
        int Index { get; }
        DateTime RecordDatetime { get; }
        string EnergyValueFormated { get; }
        float EnergyValue { get; }
        string RecordDatetimeFormated { get; }
    }
}
